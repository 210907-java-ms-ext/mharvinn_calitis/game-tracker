package revature.project2.Data;

import revature.project2.Model.Comment;

import java.util.List;

public interface CommentDAO {
    public Comment addComment(Comment comment);
    public Comment deleteComment(Comment comment);
    public List<Comment> getComment(Comment comment);
}
