package revature.project2.Data;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import revature.project2.Model.Comment;
import revature.project2.Model.Users;
import revature.project2.Service.SessionService;

import java.util.List;

public class CommentDAOImpl implements  CommentDAO{
    private static SessionFactory sf = SessionService.getSessionFactory();



    @Override
    public Comment addComment(Comment comment) {
        Session sess = sf.openSession();
        Transaction tx = sess.beginTransaction();
        int id = (int) sess.save(comment);
        comment.setCommentId(id);
        tx.commit();
        return comment;
    }

    @Override
    public Comment deleteComment(Comment comment) {
        Session sess = sf.openSession();
        Transaction tx = sess.beginTransaction();
        sess.delete(comment);
        tx.commit();
        return comment;
    }

    @Override
    public List<Comment> getComment(Comment comment) {
        Session sess = sf.openSession();

        String hql = "from Comment com where com.user = :user and com.game = :game";
        Query query = sess.createQuery(hql);
        query.setParameter("user", comment.getUser());
        query.setParameter("game", comment.getGame());

        return query.list();
    }
}
