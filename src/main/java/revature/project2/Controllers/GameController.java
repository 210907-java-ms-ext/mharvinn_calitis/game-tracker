package revature.project2.Controllers;



import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import revature.project2.Model.*;
import revature.project2.Service.GameService;
import revature.project2.Service.GameServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import java.util.List;


@RestController
@CrossOrigin
public class GameController {

    @Autowired
    private Users user;
    @Autowired
    private GameService gameService;
    @Autowired
    private Game game;
    @Autowired
    private Review review;
    @Autowired
    private Comment comment;

    @GetMapping("/games")
    public List<Game> getAllGames(){
        return  gameService.getAllGames();
    }

    @GetMapping("games/{id}")
    public ResponseEntity<Game> getGameById(@PathVariable("id")int id){
        return new ResponseEntity<>(gameService.getGameById(id), HttpStatus.OK);
    }

    @GetMapping("/games/{id}/reviews")
    public ResponseEntity<List<Review>> getGameReviews(@PathVariable("id")int id){
        game.setId(id);
        review.setGame(game);
        return new ResponseEntity<>(gameService.getReviewByGame(review),HttpStatus.OK);
    }

    @GetMapping("/games/comment")
    public ResponseEntity<List<Comment>> getComments(@RequestParam(value = "game", required = true)int gameId, @RequestParam(value = "user",required = true)int userId){
        user.setUserId(userId);
        game.setId(gameId);
        comment.setUser(user);
        comment.setGame(game);
        return new ResponseEntity<>(gameService.getComments(comment),HttpStatus.OK);
    }
    @GetMapping("/genre")
    public List<Genre> getGenre()
    {
        return gameService.getEnums();
    }
}
