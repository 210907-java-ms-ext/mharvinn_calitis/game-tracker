package revature.project2.Controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import revature.project2.Model.*;
import revature.project2.Service.UserService;
import revature.project2.Service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private Users user;
    @Autowired
    private Favorite favorite;
    @Autowired
    private Game game;
    @Autowired
    private Comment comment;
    @Autowired
    private Review review;

    @PostMapping("/login")
    public ResponseEntity <Users> login(@RequestBody Users users) throws NoSuchAlgorithmException {
        String email = users.getEmail();
        String password = users.getUserPassword();

        if(userService.login(email,password) != null){
            return new ResponseEntity<>(userService.login(email,password),HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/user/games")
    public List<Game> getUserGames(@RequestParam(value = "user_id")int id){
        user.setUserId(id);
        return userService.getUserGames(user);
    }

    @PostMapping("/register")
    public ResponseEntity <Users> register(@RequestBody Users users) throws NoSuchAlgorithmException {
        users.setRole("USER");
        return new ResponseEntity<>(userService.createUser(users), HttpStatus.CREATED);
    }

    @PostMapping("/user/favorites")
    public ResponseEntity<Favorite> addFavorite(@RequestParam(value = "Game", required = true)int gameId, @RequestParam(value = "User",required = true)int userId){
        user.setUserId(userId);
        game.setId(gameId);
        favorite.setUser(user);
        favorite.setGame(game);
        return new ResponseEntity<>(userService.addFavorite(favorite), HttpStatus.CREATED);
    }
   @DeleteMapping("/user/{id}/favorites")
   public ResponseEntity<Favorite> deleteFavorite(@PathVariable("id")int id){
        favorite.setFavId(id);
       return new ResponseEntity<>(userService.deleteFavorite(favorite), HttpStatus.OK);
   }
    @PostMapping("/user/comment")
    public ResponseEntity<Comment> addComment(@RequestBody Comment comment){
        return new ResponseEntity<>(userService.addComment(comment),HttpStatus.OK);
    }

    @DeleteMapping("/user/{id}/comment")
    public ResponseEntity<Comment> deleteComments(@PathVariable("id")int id){
        comment.setCommentId(id);
        return new ResponseEntity<>(userService.deleteComment(comment), HttpStatus.OK);
    }


   @GetMapping("/user/{id}/reviews")
   public ResponseEntity<List<Review>> getUserReview(@PathVariable("id")int id){
        user.setUserId(id);
        review.setUser(user);
        return new ResponseEntity<>(userService.getUserReview(review),HttpStatus.OK);
   }

   @PostMapping("/user/reviews")
    public ResponseEntity<Review> addReview(@RequestBody Review review){
        return new ResponseEntity<>(userService.addReview(review), HttpStatus.CREATED);
   }

   @DeleteMapping("/user/reviews")
   public ResponseEntity<Review> deleteReview(@RequestBody Review review){
       return new ResponseEntity<>(userService.deleteReview(review), HttpStatus.OK);
   }


}
