package revature.project2.Service;

import revature.project2.Model.Comment;
import revature.project2.Model.Game;
import revature.project2.Model.Genre;
import revature.project2.Model.Review;

import java.util.List;

public interface GameService {
    public Game getGameById(int id);
    public List<Comment> getComments(Comment comment);
    public List<Game> getAllGames();
    public List<Review> getReviewByGame(Review review);
    public List<Genre> getEnums();
}
