package revature.project2.Service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import revature.project2.Data.*;
import revature.project2.Model.Comment;
import revature.project2.Model.Game;
import revature.project2.Model.Genre;
import revature.project2.Model.Review;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

@Service
public class GameServiceImpl implements GameService {
    @Autowired
    private GameDAO gameDao;

    private ReviewDAO reviewDAO = new ReviewDAOImp();
    private CommentDAO commentDAO = new CommentDAOImpl();

    @Override
    public Game getGameById(int id) {
        return gameDao.getGame(id);
    }

    @Override
    public List<Comment> getComments(Comment comment) {
        return commentDAO.getComment(comment);
    }

    @Override
    public List<Game> getAllGames() {
        return gameDao.GetAllGames();
    }

    @Override
    public List<Review> getReviewByGame(Review review) {
        System.out.println(review);
        return reviewDAO.getReviewByGame(review);
    }
    @Override
    public List<Genre> getEnums()
    {
        List<Genre> allGenre = new ArrayList<Genre>(EnumSet.allOf(Genre.class));
        return allGenre;
    }
}
